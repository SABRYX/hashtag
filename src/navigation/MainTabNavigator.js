import React from "react";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createStackNavigator } from "react-navigation";

import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

//Localization
import I18n from "../localization/i18n";

/// Screens For the Tab Navigation
import { Home, Login } from "../screens";
import { NearbyGymStack } from "./NearbyGymStack";
import { Colors } from "../config/colors";

const HomeStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: I18n.t("HOME"),
      headerStyle: {
        backgroundColor: Colors.WHITE
      },
      headerTitleStyle: { alignSelf: "center" },
      headerTintColor: Colors.BLEUDEFRANCE,
      headerTitleStyle: {
        textAlign: "center",
        flex: 1
      }
    }
  }
});

const MainTabNavigation = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: I18n.t("HOME"),
        tabBarIcon: ({ tintColor }) => (
          <SimpleLineIcons size={23} name="home" color={tintColor} />
        )
      }
    },
    NearbyGyms: {
      screen: NearbyGymStack,
      navigationOptions: {
        tabBarLabel: I18n.t("NEARBY"),
        tabBarIcon: ({ tintColor }) => (
          <MaterialCommunityIcons
            size={23}
            name="weight-kilogram"
            color={tintColor}
          />
        )
      }
    }
  },
  {
    initialRouteName: "Home",
    activeColor: Colors.BLEUDEFRANCE,
    inactiveColor: Colors.GREY,
    barStyle: { backgroundColor: Colors.WHITE }
  }
);

export const MainStackNavigation = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    MainNavigation: {
      screen: MainTabNavigation
    }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);
