import React, { Component } from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  ScrollView,
  SafeAreaView,
  Dimensions
} from "react-native";
import { Card } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
const FBSDK = require("react-native-fbsdk");
const { GraphRequest, GraphRequestManager, LoginManager, AccessToken } = FBSDK;
const { width, height } = Dimensions.get("window");

class Login extends Component {
  static navigationOptions = {
    header: null
  };
  componentDidMount() {
    // this.getDataFromApi();
  }
  _responseInfoCallback(error, result) {
    if (error) {
      console.log("Error fetching data: ", error);
    } else {
      console.log("Success fetching data: ", result);
    }
  }

  getDataFromApi = () => {
    let self = this;
    LoginManager.logInWithPermissions(["public_profile"]).then(function(
      result
    ) {
      AccessToken.getCurrentAccessToken().then(token => {
        console.log("token", token.accessToken);
        self.props.navigation.navigate("MainNavigation");
      });
    });
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
      >
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Image
            source={require("../../resources/images/logo.png")}
            style={{ width: 250, height: 250 }}
          />
          <View>
            <Text style={{ fontSize: 20, fontWeight: "600", color: "#5A5C62" }}>
              Welcome to Hastag!
            </Text>
          </View>
          <View
            style={{
              width: (width * 80) / 100,
              justifyContent: "center",
              alignItems: "center",
              paddingTop: 10
            }}
          >
            <Text style={{ fontSize: 16, color: "#5A5C62" }}>
              Please sign in with Facebook
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => this.getDataFromApi()}
            style={{
              height: 36,
              width: (width * 78) / 100,
              borderRadius: 100,
              marginTop: 20
            }}
          >
            <Image
              source={require("../../resources/images/LoginwithFacebook.png")}
              style={{ width: "100%", height: "100%" }}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

export { Login };
